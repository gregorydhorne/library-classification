#!/usr/bin/env bash

################################################################################
#                                                                              #
# Classify books according to the Library of Congress Classification (LCC)     #
# system using the International System of Book Numbers (ISBN). The ISBNs can  #
# be contained in a single file or in multiple files, and referenced in        #
# category.data                                                                #
#                                                                              #
# Script Name: classifier.sh                                                   #
#                                                                              #
# Data Provider: Library of Congress, an agency of the legislative branch of   #
# the Government of the United States of America                               #
# Data Provider's Website: https://www.loc.gov                                 #
# Data retrieved from                                                          #
# https://www.loc.gov/catdir/cpso/lcco/                                        #
#                                                                              #
# Copyright (c) 2019 Gregory D. Horne                                          #
#                                                                              #
# License(s):                                                                  #
#                                                                              #
#   Source Code: MIT                                                           #
#                                                                              #
#   Source Data:                                                               #
#     Library of Congress:                                                     #
#       (https://www.loc.gov/legal/                                            #
#                                                                              #
################################################################################


GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

main()
{
  local category_file=${1}

  local data_file=""

  # Clear missing data file used to capture books for which a Library of
  # Congress classification has not been found.

  rm -f ./library/missing.data
  touch ./library/missing.data

  echo
  echo "Book Classifer"

  IFS=";"
  while read category isbn_file
  do
    data_file=${isbn_file%.*}
    data_file=${data_file%.*}.data
    retrieve_book_data ${isbn_file} ${data_file} "${category}"
    scrub_book_data ${data_file}
    echo
    IFS=";"
  done < ${category_file}
  IFS=" "

  echo
  echo "Book data retrieval completed!"
  echo
}

# Query Classify (http://classify.oclc.org), an experimental classification web
# service, developed by the Online Computer Library Center (OCLC). Data querying
# utilises the public application programming interface (API) known as OCLC
# Classify API.

# Retrieve author name, book title, and OCLC Work ID (OWI) querying by ISBN.

query_by_isbn()
{
	local isbn_file=${1}
	local data_file=${2}

  IFS=" "
  while read isbn
  do
    # Display activity indicator.

    echo -e -n "${GREEN}+${NC}"

    # Throttle data retrieval to avoid possibly having IP address blocked and to
    # be a good data citizen. Use parameters 0 and 0 if no delay is desired.

    sleep $(random_number 0 3)

    # Query classification service and retrieve book data by ISBN.

    curl \
      -X GET \
      -H "Content-type: application/xml" \
      -H "Accept: application/xml" \
      -s \
      "http://classify.oclc.org/classify2/Classify?isbn=${isbn}" | \

    # Extract author name, book title, and OCLC work identifier associated
    # with current ISBN. If there are multiple matching records, select the
    # last entry. Caveat: Record order inconsistencies returned by OCLC Classify
    # neccesitate a seconday query using the reterieved OWI attribute.

    mawk -v isbn=${isbn} '
      BEGIN { FS = ";"; OFS = "@"; matched = 0; seen = 0; }

      # Seek last record matching criteria.

      {
        if ($0 ~ /work author/) {
          seen++;
          if (seen == 1) {
            matched = 1;
            author = $0;
            gsub(/.*work author="|.*work editions=.+|" .*/, "", author);
            owi = $0;
            gsub(/.*owi="|".*/, "", owi);
            title = $0;
            gsub(/.*title="|".*/, "", title);
            lcc = "";
            #print("1.", isbn, author, title, owi);
            seen = 0;
          }
        }

        # Extract author name, book title, and OWI.

        if ($0 ~ /mostRecent holdings/ && $0 ~ /[A-Z]+[0-9}+]/) {
          sub(/.*=\"/, "", $0);
          sub(/\".*/, "", $0);
          #print("2.", $0);
          lcc = $0;
          matched = 2;
        }

        if ($0 ~ /work author/ && $0 ~ /LCC/) {
          seen++;
          if ( seen == 1 ) {
            matched = 2;
            author = $0;
            gsub(/.*work author="|.*work editions=.+|" .*/, "", author);
            owi = $0;
            gsub(/.*owi="|".*/, "", owi);
            title = $0;
            gsub(/.*title="|".*/, "", title);
            lcc = "";
          }
        }

        if ( matched == 2 ) {
          matched++;
          print(lcc, isbn, author, title, owi);
          if (lcc) {
            seen = 0;
          }
        }
      }
    ' >> ${data_file}
  done < ${isbn_file}
  IFS=" "
}

# Retrieve author name, book title, and Library of Congression classification
# querying by previously retrieved OWI.

query_by_owi()
{
  local data_file=${1}

  local temp_file=${data_file}.tmp

  rm -f ${temp_file}

  IFS="@"
  while read lcc isbn author title owi
  do
    # Display activity indicator.

    echo -e -n "${CYAN}+${NC}"

    # Throttle data retrieval to avoid possibly having IP address blocked and to
    # be a good data citizen. Use parameters 0 and 0 if no delay is desired.

    sleep $(random_number 1 5)

    # Query classification service and retrieve book data by OWI.

    curl \
      -X GET \
      -H "Content-type: application/xml" \
      -H "Accept: application/xml" \
      -s \
      "http://classify.oclc.org/classify2/ClassifyDemo?owi=${owi}" | \

    # Extract author name, book title, and Library of Congress classification
    # associated with current OWI. Pass along any previously retrieved ISBN,
    # author name, and book title.

    mawk -v lcc=${lcc} -v isbn=${isbn} -v author="${author}" -v title="${title}" '
      BEGIN { FS = " "; OFS = "@"}

      # Seek section of retrieved webpage containing author name or book title.
      # This section is not always present which necessotates retrieving an
      # alternate webpage identified within current webpage; not implemented
      # yet.

      /<dd>/ {
        seen++;

        # Extract book title and capitalise first letter of each word in book
        # title after normalising by converting entire title to lowercase.

        if (seen == 1) {
          title = $0;
          gsub(/ *<[^>]*> */,"", title);
          gsub(/\"/, "", title);

          title = tolower(title);
          n = split(title, words, " ");
          title = "";
          for(i = 1; i <= n; i++) {
            title = title" "toupper(substr(words[i], 1, 1)) substr(words[i], 2);
          }
          sub(/^ /, "", title);
        }

        # Extract author name, if present.

        if (seen == 2) {
          author = $0;
          gsub(/ *<[^>]*> */,"", author);
        }
      }

      # Extract Library of Congress classification, if present.

      #$0 ~ /<td>[A-Z]{1,}[0-9]+/ {
      $0 ~ /<td>[A-Z]+[0-9]+/ {
        lcc = $0;
        gsub(/<[^>]*>/, "", lcc);
      }

      END {
        print(lcc, author, title, isbn);
        if (length(lcc) == 0) {
          print(isbn, author, title) >> "./library/missing.data";
        }
      }
    ' >> ${temp_file}
  done < ${data_file}
  IFS=" "

  mv ${temp_file} ${data_file}
}

# Generate a random number between lower bound and upper bound, inclusive.

random_number()
{
  local lower_bound=${1}
  local upper_bound=${2}

  echo $(
    mawk -v min=${lower_bound} -v max=${upper_bound} '
      BEGIN {
        srand();
        print(int(min + rand() * (max - min + 1)));
      }
    '
  )
}

# Retrieve data about each book, querying by ISBN and subsequently by OWI to
# obtain name of the author, title of the book, and Library of Congress
# classification.

retrieve_book_data()
{
  local isbn_file=${1}
  local data_file=${2}
  local category=${3}

  echo
  echo -n 'Retrieving author, title, and classification'
  echo " for ${category} books"

  rm -f ${data_file}

  query_by_isbn ${isbn_file} ${data_file}
  query_by_owi ${data_file}
}

# Clean-up various artefacts in the retrieved book data.

scrub_book_data()
{
  local data_file=${1}

  local temp_file=${data_file}.tmp

  mawk '
    {
      # Remove dates of birth and death.

      # gsub(/, [0-9]{4}-[0-9]*/, "", $0);
      gsub(/, [0-9]+-[0-9]*/, "", $0);

      # Remove bracketed authorship or editoral role.

      #gsub(/\s\[[A-Z|a-z|]+\;*\s*[A-Z|a-z]+\]/, "", $0);
      gsub(/ \[[A-Z|a-z|]+\;* *[A-Z|a-z]+\]/, "", $0);

      # Remove author list separated by pipe character.

      #gsub(/\s+\|\s+/, ";", $0); sub(/\s@/, "@", $0);
      gsub(/ +\| +/, ";", $0); sub(/ @/, "@", $0);

      # Remove miscellaneous extraneous artefacts.

      sub("--cover", "", $0);

      print($0);
    }
  ' ${data_file} > ${temp_file}

  mv ${temp_file} ${data_file}
}

###############################################################################

main ./library/category.data

exit 0
