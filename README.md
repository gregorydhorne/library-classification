# Personal/Professional Library

Use the Library of Congress Classification (LCC) system to organise your
personal or professional book collection / library.

## Preliminary Steps

The first step, which only needs to be performed one time, is retrieving the
Library of Congress Classification top-level class number and descriptive text.

```
$ ./scraper.sh

Retrieved 21 classification classes from the Library of Congress, an agency of
the legislative branch of the Government of the United States of America
```

The second step involves adding books to the .isbn.data files and groupings to
the category.data file. Update the .isbn.data and category.data files when books
are added to your library.

Given an International Standard Book Number (ISBN) the title, author, and
Library of Congress Classification (LCC) can be retrieved. There are two primary
ways to store the ISBNs: a single file, such as library.isbn.data, or multiple
files by field of study, subject, etc., such as computer-science.isbn.data. File
names are user selectable, however, the *.isbn.data* extension must be used.

**Example**

File: computer-science.isbn.data

158488360X
9780997316001

File: forensic-science.isbn.data

9781420088267
0133268500

In either case the data file name(s) must be added to the category file,
category.data. The order in which the file names are listed in the category
file determines the order these groupings will appear in the library file by
default.

**Example**

File: category.data

Forensic Science;forensic-science.isbn.data      
Computer Science;computer-science.isbn.data

With the .isbn.data and category.data file populated, selected attributes about
each book can potentially be retrieved.

# Classify Books by Library of Congress Classification

The third step retrieves data about each book if possible.

**Example**

```
$ ./classifier.sh

Book Classifier

Retrieving author, title, and classification for Forensic Science books
..

Retrieving author, title, and classification for Computer Science books
..

Book data retrieval completed!
```

For each .isbn.data file an associated .data is produced. A representative
sample (158 books) from my professional library successfully matched 98.7% of
the ISBNs to the correct author, title, and Library of Congress classification.
See the **Issues** section for details.

Rerun the book classifier when books are added to your library.

**Example**

*Note: Data about each book is on a single line in the .data file.*

File: forensic-science.data

HV8073@Curran, James Michael@Introduction To Data Analysis For Forensic
	Scientists@9781420088267
HV8073@Saferstein, Richard@Forensic Science Handbook@0133268500

File: computer-science.data

QA76@Tucker, Allen B.@Computer Science Handbook@158488360X
QA76.9.A43@Ferreira Filho, Wladston@Computer Science Distilled : Learn The Art
	Of Solving Computational Problems@9780997316001

A single .isbn.data file can be used for all books if desired. The category,data
file is mandatory and may reference the single .isbn.data file.

## Book Entry Produced by the Library Formatter using User-Defined Headings

The library formatter takes as input a .data file containing data about one or
more books and produces an HTML-formatted or markdown-formatted file.

Process a single .data file invoke the library formatter as shown.

```
$ ./formatter.sh -c forensic-science.data -t html

Library Formatter

Library [library.html] successfully created
```

<h3>Forensic Science</h3>
<ul>
<!-- HV8073 --> <li>Curran, James Michael &ndash; Introduction To Data Analysis
	For Forensic Scientists&nbsp;&nbsp;&nbsp;&nbsp;[ISBN: 9781420088267]</li>
<!-- HV8073 --> <li>Saferstein, Richard &ndash; Forensic Science Handbook
	&nbsp;&nbsp;&nbsp;&nbsp;[ISBN: 0133268500]</li>
</ul>

Process all .data files contained in the category file invoke the library
formatter as shown.

```
$ ./formatter.sh -a

Library Formatter

Library [library.html] successfully created
```

<h3>Forensic Science</h3>
<ul>
<!-- HV8073 --> <li>Curran, James Michael &ndash; Introduction To Data Analysis
	For Forensic Scientists&nbsp;&nbsp;&nbsp;&nbsp;[ISBN: 9781420088267]</li>
<!-- HV8073 --> <li>Saferstein, Richard &ndash; Forensic Science Handbook
	&nbsp;&nbsp;&nbsp;&nbsp;[ISBN: 0133268500]</li>
</ul>

<h3>Computer Science</h3>
<ul>
<!-- QA76 --> <li>Tucker, Allen B. &ndash; Computer Science Handbook
	&nbsp;&nbsp;&nbsp;&nbsp;[ISBN: 158488360X]</li>
<!-- QA76.9.A43 --> <li>Ferreira Filho, Wladston &ndash; Computer Science
	Distilled : Learn The Art Of Solving Computational Problems
		&nbsp;&nbsp;&nbsp;&nbsp;[ISBN: 9780997316001]</li>
</ul>

## Book Entry Produced by the Library Formatter using Standard Headings

```
$ ./formatter.sh -a -l

Library Formatter

9 book collection(s) merged successfully to create library [library.html]
```

<h3>Social Sciences</h3>
<ul>
<!-- HV8073 --> <li>Curran, James Michael &ndash; Introduction To Data Analysis
	For Forensic Scientists&nbsp;&nbsp;&nbsp;&nbsp;[ISBN: 9781420088267]</li>
<!-- HV8073 --> <li>Saferstein, Richard &ndash; Forensic Science Handbook
	&nbsp;&nbsp;&nbsp;&nbsp;[ISBN: 0133268500]</li>
</ul>

<h3>Science</h3>
<ul>
<!-- QA76 --> <li>Tucker, Allen B. &ndash; Computer Science Handbook
	&nbsp;&nbsp;&nbsp;&nbsp;[ISBN: 158488360X]</li>
<!-- QA76.9.A43 --> <li>Ferreira Filho, Wladston &ndash; Computer Science
	Distilled : Learn The Art Of Solving Computational Problems
		&nbsp;&nbsp;&nbsp;&nbsp;[ISBN: 9780997316001]</li>
</ul>

## General Usage Information

Prior to executing the library formatter both the Library of Congress
classification scraper and the book classifier must be executed.

The library formatter is highly configurable and the source code easily
modifiable with liberal explanatory comments.

```
$ ./formatter.sh -h

formatter.sh -c filename [-a] [-f library_filename] [-t format ] [-l] [-h]

-a: concatenate all library data files (optional, overrides -c, defaults to
disabled (0))
-c: a single book collection data file (mandatory, unless overridden by -a)
-f: library name (optional, defaults to library.html)
-l: use Library of Congress Classification class descriptive text as headers
(optional, defaults to disabled (0))
-t: format type of library (optional, defaults to html)
-h: brief usage information (optional, displays supported options)
```

# Issues

A representative sample (158 books) from my professional library produced:

- 2 books missing a Library of Congress classification

The books with a missing Library of Congress Classification are a consequence
of the variation in the responses received to a GET request. Specifically,
one response provides the requested information, while the other response
results in multiple URLs leading to the requested information. (TODO)

# Legal Notice

The source code is licenced under the MIT Licence and copyrighted. See the
LICENCE file for terms and conditions.
