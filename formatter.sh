#!/usr/bin/env bash

################################################################################
#                                                                              #
# Create a listing of books in the library, produced by the book collection    #
# classifier, as a single file formatted using HTML or markdown.               #
#                                                                              #
# Script Name: formatter.sh                                                    #
#                                                                              #
# Copyright (c) 2019 Gregory D. Horne                                          #
#                                                                              #
# License(s):                                                                  #
#                                                                              #
#   Source Code: MIT                                                           #
#                                                                              #
################################################################################


# Default Configuration Option Settings

concatenate_enable=0        # concatenate library data flag
data_file=""                # single book collection, not entire library
format="html"               # generated library file format
lcc_category_enable=0       # use LCC categories instead of user-defined
library_file="./library/library."${format}	# generated library file name


# Consolidate all book collections into an unified library.

concatenate()
{
  local category_file=${1}
  local library_file=${2}
  local use_lcc_headers=${3}
  local format=${4}

  local counter=0
  local data_file=""
  local library_mode=1

  library_file=${library_file%.*}.${format}

  # Remove existing library.

  rm -f ${library_file}

  # Process entire library rather than a single book collection.
  # Delay labeling if Library of Congress classification descriptive text is to
  # be used instead of custom labeling.

  IFS=";"
  while read category data_file
  do
    if [ ${use_lcc_headers} -eq 0 ]
    then
      data_file=${data_file%.isbn*}.data
      format_library \
      	${data_file} ${format} ${data_file} \
      	${library_mode} ${use_lcc_headers}
      if [ -e ${data_file} ] && [ -e ${data_file%.*}.${format} ]
      then
        cat ${data_file%.*}.${format} >> ${library_file}
        rm -f ${data_file%.*}.${format}
        counter=$((counter+1))
      fi
    else
      data_file=${data_file%.isbn*}.data
      if [ -e ${data_file} ]
      then
        cat ${data_file} >> ${library_file%.*}.data
        counter=$((counter+1))
      fi
    fi
    IFS=";"
  done < ${category_file}
  IFS=" "

  # Apply Library of Congress classification descriptive text as headers to
  # unified library.

  if [ ${use_lcc_headers} -eq 1 ]
  then
    format_library \
      ${library_file%.*}.data ${format} ${library_file} \
      ${library_mode} ${use_lcc_headers}
    rm -f ${library_file%.*}.data
  fi

  # Display status of library creation.

  if [ ${counter} -gt 0 ] && [ -e ${library_file} ]
  then
    echo -n "${counter} book collection(s) "
    if [ ${use_lcc_headers} -eq 0 ]
    then
      echo -n "completed and "
    fi
      echo -n "merged successfully to create library [${library_file}]"
      echo
  else
    echo "ERROR: [${library_file}] not created"
  fi
}

# Dispatch library formatter and display final status.

format_library()
{
  local data_file=${1}
  local format=${2}
  local library_file=${3}
  local library_mode=${4}
  local use_lcc_headers=${5}

  library_file=${library_file%.*}.${format}

  # Remove existing library unless building a complete library or not using
  # Library of Congress classifications.

  if [ ${use_lcc_headers} -eq 1 ] && [ ${library_mode} -eq 0 ]
  then
    rm -f ${library_file}
  fi

  # Apply desired formatting style for headings and books.

  case ${format} in
    html)
      html_format ${data_file} ${library_file} ${use_lcc_headers}
      ;;
    md)
      markdown_format ${data_file} ${library_file} ${use_lcc_headers}
      ;;
    *)
      echo "ERROR: unsupported format: ${format}"
      ;;
  esac

  # Display status of library creation.

  if [ -e ${library_file} ] && [ ${library_mode} -eq 0 ]
  then
    echo "Library [${library_file}] successfully created"
  elif [ ! -e ${library_file} ]
  then
    echo "ERROR: [${library_file}] not created"
  fi
}

# Generate an HTML-formatted entry for each book in current collection or
# library.

html_format()
{
  local data_file=${1}
  local library_file=${2}
  local use_lcc_headers=${3}

  local category=""
  local header=""
  local letter=""

  sort_library ${data_file}

  # Mark beginning of category when using user-defined categories.

  if [ ${use_lcc_headers} -eq 0 ]
  then
    local header=$(grep ${data_file%.*} ./library/category.data | cut -d \; -f 1)
    echo "<h3>${header}</h3>" > ${library_file}
    echo "<ul>" >> ${library_file}
  fi

  IFS="@"
  while read lcc author title isbn
  do
    # Mark beginning and end of category when using Library of Congress
    # classifications.

    if [ ${use_lcc_headers} -eq 1 ] && [ ! -z ${lcc} ]
    then
      letter=$(echo ${lcc} | sed 's/./&,/g' | cut -d \, -f 1)
      if [[ ${letter} != ${category} ]]
      then
        if [ ! -z ${category} ]
        then
          echo "</ul>" >> ${library_file}
          echo "" >> ${library_file}
        fi
        header=$(grep ^${letter} ./library/lcc.data | cut -d \, -f 2-)
        category=${letter}
        echo "<h3>${header}</h3>" >> ${library_file}
        echo "<ul>" >> ${library_file}
      fi
    fi

    # Filter out books without a Library of Congress classification.
    if [ ! -z ${lcc} ]
    then
      echo -n "<!-- ${lcc} --> <li>${author} &ndash; ${title}" >> ${library_file}
      echo "&nbsp;&nbsp;&nbsp;&nbsp;[ISBN: ${isbn}]</li>" >> ${library_file}
    fi

  done < ${data_file}
  IFS=" "

  # Mark end of final category.

  echo "</ul>" >> ${library_file}
  echo "" >> ${library_file}
}

# Generate a markdown-formatted entry for each book in current collection or
# library.

markdown_format()
{
  local data_file=${1}
  local library_file=${2}
  local use_lcc_headers=${3}

  local category=""
  local header=""
  local letter=""

  sort_library ${data_file}

  # Mark beginning of category when using user-defined categories.

  if [ ${use_lcc_headers} -eq 0 ]
  then
    local header=$(grep ${data_file%.*} ./library/category.data | cut -d \; -f 1)
    echo "### ${header}" > ${library_file}
    echo "" >> ${library_file}
  fi

  IFS="@"
  while read lcc author title isbn
  do
    # Mark beginning and end of category when using Library of Congress
    # classifications.

    if [ ${use_lcc_headers} -eq 1 ] && [ ! -z ${lcc} ]
    then
      letter=$(echo ${lcc} | sed 's/./&,/g' | cut -d \, -f 1)
      if [[ ${letter} != ${category} ]]
      then
        if [ ! -z ${category} ]
        then
          echo "" >> ${library_file}
        fi
        header=$(grep ^${letter} ./library/lcc.data | cut -d \, -f 2-)
        category=${letter}
        echo "### ${header}" >> ${library_file}
        echo "" >> ${library_file}
      fi
    fi

    # Filter out books without a Library of Congress classification.
    # The Library of Congress classification is wrapped in a tag specific to
    # the static website generator Hugo which uses the Go language.

    if [ ! -z ${lcc} ]
    then
      echo -n "- {{/* ${lcc} */}} ${author} &ndash; ${title}" >> ${library_file}
      echo "&nbsp;&nbsp;&nbsp;&nbsp;[ISBN: ${isbn}]" >> ${library_file}
    fi

  done < ${data_file}
  IFS=" "

  # Mark end of final category.

  echo "" >> ${library_file}
}

main()
{
  local concatenate_enable=${1}
  local library_file=${2}
  local use_lcc_headers=${3}
  local format=${4}
  local data_file=${5}

  validate_options ${concatenate_enable} ${format} ${data_file}

  echo
  echo "Library Formatter"
  echo

  if [ ${concatenate_enable} -eq 0 ]
  then
    format_library \
      ${data_file} ${format} ${library_file} \
      ${concatenate_enable} ${use_lcc_headers}
  else
    concatenate \
      ./library/category.data ${library_file} ${use_lcc_headers} ${format}
  fi

  echo
}

# Sort books by Library of Congress classification.

sort_library()
{
  local data_file=${1}

  local temp_file=${data_file}.tmp

  # Verify whether the collection or library exists.

  if [ ! -e ${data_file} ]
  then
    echo "ERROR: ${data_file} does not exist"
    exit 1
  fi

  # Pad entries lacking a Library of Congress classification to force
  # them to end of listing. If the book classifier generated the data file,
  # then there should be no books in the data file with a missing class number.
  # Any book for which the Library of Congress classification cannot be
  # determined would have been redirected to the missing data file by the
  # classifier.

  sed 's/^@/ZULU@/g' ${data_file} | \
  sort -d -k 1 -t @ | \
  sed 's/ZULU//g' > ${temp_file}
  mv ${temp_file} ${data_file}
}

# Display supported options and associated arguments.

usage()
{
  echo
  echo -n "formatter.sh -c filename"
  echo " [-a] [-f library_filename] [-t format ] [-l] [-h]"
  echo
  echo -n "-a: concatenate all library data files"
  echo " (optional, overrides -c, defaults to disabled (0))"
  echo -n "-c: a single book collection data file"
  echo " (mandatory, unless overridden by -a)"
  echo "-f: library name (optional, defaults to library.html)"
  echo -n "-l: use Library of Congress Classification class descriptive text as"
  echo " headers (optional, defaults to disabled (0))"
  echo "-t: format type of library (optional, defaults to html)"
  echo "-h: brief usage information (optional, displays supported options)"
  echo
}

# Validate selected option settings, report and errors, and terminate if any
# invalid option arguments are detected.

validate_options()
{
  local concatenate_enable=${1}
  local format=${2}
  local data_file=${3}

  # Verify a data file is present unless all data files are to be processed.

  if [ ${concatenate_enable} -eq 0 ] && [ -z ${data_file} ]
  then
    echo "ERROR: filename required"
    exit 1
  fi

  # Verify library format.

  if [ "${format}" != "html" ] && [ "${format}" != "md" ]
  then
    echo "ERROR: unsupported format: ${format}"
    exit 1
  fi
}


################################################################################

# Parse command-line arguments and set options accordingly.

while [ "${1}" != "" ]
do
  case ${1} in
    -a)                     # optional, overrides -d, defaults to disabled (0)
      concatenate_enable=1
      data_file=""
      ;;
    -c)                     # mandatory, unless overridden by -a
     	shift
     	if [ ${concatenate_enable} -eq 0 ]
     	then
       	data_file=./library/${1}
     	fi
     	;;
   	-f)                     # optional, defaults to library.html
     	shift
     	library_file=./library/${1%.*}.${format}
     	;;
    -l)                     # optional, defaults to disabled (0)
     	lcc_category_enable=1
     	;;
    -t)                     # optional, defaults to html
     	shift
     	format=${1}
     	if [ "${format}" = "markdown" ]
     	then
       	format="md"
     	fi
     	;;
   	-h)
     	usage                 # optional, overrides any other option
     	exit 0
     	;;
   	*)
     	usage
     	exit 1
 	esac
 	shift
 done

################################################################################


main \
  ${concatenate_enable} ${library_file} \
  ${lcc_category_enable} ${format} ${data_file}

exit 0
