#!/usr/bin/env sh

################################################################################
#                                                                              #
# Scrape the Library of Congress Classifications (LCC) from the Library of     #
# Congress website.                                                            #
#                                                                              #
# Script Name: scraper.sh                                                      #
#                                                                              #
# Data Provider: Library of Congress, an agency of the legislative branch of   #
# the Government of the United States of America                               #
# Data Provider's Website: https://www.loc.gov                                 #
# Data retrieved from                                                          #
# https://www.loc.gov/catdir/cpso/lcco/
#                                                                              #
# Copyright (c) 2019 Gregory D. Horne                                          #
#                                                                              #
# License(s):                                                                  #
#                                                                              #
#   Source Code: MIT                                                           #
#                                                                              #
#   Source Data:                                                               #
#     Library of Congress:                                                     #
#       (https://www.loc.gov/legal/                                            #
#                                                                              #
################################################################################


# Retrieve Library of Congress classifications.

curl -s https://www.loc.gov/catdir/cpso/lcco/ | \

# Remove CTRL-M characters from retrieved webpage.
sed 's///g' | \

# Extract Library of Congress classifications, top-level only. The descriptive
# classification text contains forced line breaks for some entries.

mawk '
	BEGIN { FS = "-WP"; }

  $0 ~ /[A-Z] -- / {
		gsub(/ *<[^>]*> */,"", $1);
		sub(/ -- /, ",", $1);
    gsub(/\. +/, ", ", $1);
		if ($1 ~ /^D/) {
      $1 = $1" EUROPE, ASIA, AFRICA, AUSTRALIA, NEW ZEALAND, ETC."
    }
		if ($1 ~ /^G/) {
      $1 =  $1"RECREATION";
    }
		if ($1 ~ /^Z/) {
      $1= $1" RESOURCES (GENERAL)";
    }
		gsub(/\-.*/, "", $1);
		print($1);
	}
' | \

# Convert descriptive text to lowercase except the first character of each word.

sed 's/\b\([[:alpha:]]\)\([[:alpha:]]*\)\b/\u\1\L\2/g' > ./library/lcc.data

echo
echo -n "Retrieved $(wc -l ./library/lcc.data | cut -d \  -f 1) classification classes"
echo " from the Library of Congress, an agency of"
echo "the legislative branch of the Government of the United States of America"
echo

exit 0
